package com.somospnt.registrate.demo.service;

import com.somospnt.registrate.demo.domain.Usuario;

public interface UsuarioService {

    Usuario guardar(Usuario usuario);
}
