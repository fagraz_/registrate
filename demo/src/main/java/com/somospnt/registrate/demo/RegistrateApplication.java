package com.somospnt.registrate.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegistrateApplication {

    public static void main(String[] args) {
        SpringApplication.run(RegistrateApplication.class, args);
    }
}