package com.somospnt.registrate.demo.service.impl;

import com.somospnt.registrate.demo.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import com.somospnt.registrate.demo.repository.UsuarioRepository;
import com.somospnt.registrate.demo.service.UsuarioService;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UsuarioServiceImpl implements UsuarioService {

    private static final String CARACTERES_ESPECIALES = "!#$%&/()=?¡¿'][{}+´¨*";
    private static final String ABECEDARIO = "abcdefghijklmñopqrstuvwxyz";
    private static final String NUMEROS = "0123456789";

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public Usuario guardar(Usuario usuario) {

        if (usuario == null) {
            throw new IllegalArgumentException("El usuario no ha podido cargarse correctamente.");
        }
         
        if (usuario.getEmail() == null) {
            throw new IllegalArgumentException("El email no puede ser nulo.");
        }
        if (!esEmailValido(usuario.getEmail())) {
            throw new IllegalArgumentException("El formato del email es erróneo.");
        }
        if (usuario.getEmail().contains(" ")) {
            throw new IllegalArgumentException("El email no puede tener espacios");
        }
        
        String password = usuario.getPassword();

        if (password == null) {
            throw new IllegalArgumentException("La password no puede ser nula.");
        }
       
        if (password.length() < 6) {
            throw new IllegalArgumentException("La password debe tener mas de 6 caracteres");
        }
        verificarEmailRepetido(usuario.getEmail());
        verificarPasswordTipoDeCaracteres(password, CARACTERES_ESPECIALES);
        verificarPasswordTipoDeCaracteres(password, ABECEDARIO);
        verificarPasswordTipoDeCaracteres(password, NUMEROS);
        return usuarioRepository.save(usuario);
    }

    private boolean esEmailValido(String mail) {
        try {
            InternetAddress emailAddress = new InternetAddress(mail);
            emailAddress.validate();
        } catch (AddressException errorDeFormatoMail) {
            return false;
        }
        return true;
    }

    private void verificarPasswordTipoDeCaracteres(String password, String caracteres) {
        boolean tieneCaracter = false;
        for (int i = 0; i < caracteres.length(); i++) {
            if (password.indexOf(caracteres.charAt(i)) >= 0) {
                tieneCaracter = true;
                break;
            }
        }
        if (tieneCaracter == false) {
            throw new IllegalArgumentException("La contraseña debe tener los caracteres necesarios para ser correcta.");
        }
    }

    private void verificarEmailRepetido(String email) {
        if (!usuarioRepository.findByEmail(email).isEmpty()) {
            throw new IllegalArgumentException("El mail ingresado ya existe.");
        }
    }

}
