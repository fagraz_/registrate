package com.somospnt.registrate.demo.controller;

import com.somospnt.registrate.demo.domain.Usuario;
import com.somospnt.registrate.demo.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @RequestMapping("/")
    public String index() {
        return "usuario";
    }

    @PostMapping("/cargarusuario")
    public String cargarUsuario(@ModelAttribute("email") String email, @ModelAttribute("password") String password, Model model) {
        Usuario usuario = new Usuario();
        usuario.setEmail(email);
        usuario.setPassword(password);
        try {
            usuarioService.guardar(usuario);
            model.addAttribute("mostrarMensajeEmail", email);
        } catch (IllegalArgumentException excepcion) {
            model.addAttribute("mensajeError", excepcion.getMessage());
        }
        return "usuario";
    }

}
