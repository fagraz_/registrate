package com.somospnt.registrate.demo.service;

import com.somospnt.registrate.demo.DemoApplicationTests;
import com.somospnt.registrate.demo.domain.Usuario;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class UsuarioServiceTest extends DemoApplicationTests {

    @Autowired
    private UsuarioService usuarioService;

    @Test
    public void guardar_conUsuarioOK_devuelveUsuarioCreado() {
        Usuario usuarioAGuardar = new Usuario();

        usuarioAGuardar.setEmail("usuario@mail.com");
        usuarioAGuardar.setPassword("123456!aa");

        Usuario usuarioGuardado = usuarioService.guardar(usuarioAGuardar);
        assertTrue(usuarioGuardado.getId() != null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardarUsuario_conUsuarioNull_lanzaExcepcion() {
        Usuario usuario = null;
        usuarioService.guardar(usuario);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardarUsuario_SinMail_lanzaExcepcion() {
        Usuario usuarioAGuardar = new Usuario();

        usuarioService.guardar(usuarioAGuardar);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardarUsuario_conMailVacio_lanzaExcepcion() {
        Usuario usuarioAGuardar = new Usuario();
        String emailEspaciosEnBlanco = "       ";
        usuarioAGuardar.setEmail(emailEspaciosEnBlanco.trim());

        usuarioService.guardar(usuarioAGuardar);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardarUsuario_conMailSinArroba_lanzaExcepcion() {
        Usuario usuarioAGuardar = new Usuario();

        usuarioAGuardar.setEmail("franciscohotmail.com");

        usuarioService.guardar(usuarioAGuardar);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardarUsuario_conMasDeUnaArroba_lanzaExcepcion() {
        Usuario usuarioAGuardar = new Usuario();

        usuarioAGuardar.setEmail("francisco@@@@@@hotmail.com");

        usuarioService.guardar(usuarioAGuardar);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardarUsuario_conMailSinUserName_lanzaExcepcion() {
        Usuario usuarioAGuardar = new Usuario();

        usuarioAGuardar.setEmail("@.com");

        usuarioService.guardar(usuarioAGuardar);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardarUsuario_conMailHastaArroba_lanzaExcepcion() {
        Usuario usuarioAGuardar = new Usuario();

        usuarioAGuardar.setEmail("usuario@");

        usuarioService.guardar(usuarioAGuardar);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardarUsuario_conMailSoloArroba_lanzaExcepcion() {
        Usuario usuarioAGuardar = new Usuario();

        usuarioAGuardar.setEmail("@");

        usuarioService.guardar(usuarioAGuardar);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardarUsuario_conUsuarioSinPassword_lanzaExcepcion() {
        Usuario usuarioAGuardar = new Usuario();
        usuarioAGuardar.setEmail("a@a");

        usuarioService.guardar(usuarioAGuardar);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardarUsuario_conPasswordMenosDe6Caracteres_lanzaExcepcion() {
        Usuario usuarioAGuardar = new Usuario();

        usuarioAGuardar.setEmail("a@a.com");
        usuarioAGuardar.setPassword("a");

        usuarioService.guardar(usuarioAGuardar);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardarUsuario_conPasswordSinCaracterEspecial_lanzaExcepcion() {
        Usuario usuarioAGuardar = new Usuario();

        usuarioAGuardar.setEmail("a@a.com");
        usuarioAGuardar.setPassword("123aaa");

        usuarioService.guardar(usuarioAGuardar);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardarUsuario_conPasswordSinLetras_lanzaExcepcion() {
        Usuario usuarioAGuardar = new Usuario();

        usuarioAGuardar.setEmail("a@a");
        usuarioAGuardar.setPassword("12345%6");

        usuarioService.guardar(usuarioAGuardar);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardarUsuario_conPasswordSinNumeros_lanzaExcepcion() {
        Usuario usuarioAGuardar = new Usuario();

        usuarioAGuardar.setEmail("a@a");
        usuarioAGuardar.setPassword("aaaaa$$");

        usuarioService.guardar(usuarioAGuardar);
    }

    @Test
    public void guardarUsuario_conEmailRepetido_lanzaExcepcion() {
        Usuario usuarioAGuardar = new Usuario();

        usuarioAGuardar.setEmail("email@repetido.comm");
        usuarioAGuardar.setPassword("aaaa!1");
        try {
            usuarioService.guardar(usuarioAGuardar);
        } catch (IllegalArgumentException ex) {
            assertEquals("El mail ingresado ya existe.", ex.getMessage());
        }
    }
    
}
