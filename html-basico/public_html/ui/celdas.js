function borrarCeldas() {
    var celdasPadre = $("#fila-celdas");
    celdasPadre.empty();
}

function generarCeldas() {
    borrarCeldas();
    var celdas = $("#input-numero-celdas").val();
    if (celdas > 500) {
        return alert("No se pueden poner mas de 500 celdas!!!");
    }
    for (var i = 1; i <= celdas; i++) {
        var div = document.createElement("div");

//        if ((i % 2    ) === 0) {
//            div.className = 'celda col-lg-1 text-center par';
//        } else {
//            div.className = 'celda col-lg-1 text-center impar';
//        }
        div.className = 'celda col-lg-1 text-center';
        var textoCelda = document.createTextNode(i);
        div.appendChild(textoCelda);
        $("#fila-celdas").append(div);
    }
}
function eventoBoton() {
    $("#boton-generar-celdas").click(generarCeldas);
}

//$("DOMContentLoaded").ready.eventoBoton;
//document.addEventListener("DOMContentLoaded", eventoBoton);
$(document).ready(eventoBoton); 