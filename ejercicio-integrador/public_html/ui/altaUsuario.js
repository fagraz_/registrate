var mails = new Array();
function init() {
    var mailIngresado = $(".js-email-usuario").val();
    console.log(mailIngresado);
    bindearEventoClickSubmit();
}

function validarCampos() {
    var mailIngresado = $(".js-email-usuario").val();
    var password = $("#password").val();
    verificarInputMailVacio(mailIngresado);
    verificarFormatoMail(mailIngresado);
    validarMailRepetido(mailIngresado);
    verificarInputPasswordVacio(password);
    verificarCantidadDeCaracteresPassword(password);
    validarPasswordTipoDeCaracteres(password);
    validarPasswordRepetida(password);
    mails.push(mailIngresado);
    reiniciarInputs();
}

function verificarInputPasswordVacio(pass) {
    $(".js-contenido-password").empty();
    if (pass === "") {
        $(".js-contenido-password").append("<div class='card text-white bg-danger' style='max-width: 20rem;'>\n\
<div class='card-header'>El campo contraseña esta vacio</div></div>");
        throw new Error("El campo contraseña esta vacío.");
    }
}

function validarPasswordTipoDeCaracteres(password) {
    verificarCaracteresEspeciales(password);
    verificarAbc(password);
    verificarPasswordConNumeros(password);
}

function validarPasswordRepetida(password) {
    $(".js-contenido-passwordver").empty();
    var passwordVerificar = $("#password-verificar").val();
    if (password !== passwordVerificar) {
        $(".js-contenido-passwordver").append("<div class='pass card text-white bg-danger' style='max-width: 20rem;\n\
'><div class='card-header'>Las contraseñas deben coincidir.</div></div>");
        throw new Error("Las contraseñas no coinciden");
    }
}

function validarMailRepetido(mailIngresado) {
    $("errorMail").remove();
    mails.forEach(function (element) {
        if (element === mailIngresado) {
            $(".js-email").html("¡No puede haber usuarios repetidos!</small><div class='card text-white bg-danger' style='max-width: 20rem;'><div class='errorMail card-header'>Ese mail ya ha sido ingresado.</div></div>");
            throw new Error("El mail ingresado ya esta registrado.");
        }
    });
}



function bindearEventoClickSubmit() {
    $(".js-boton-registrarme").click(validarCampos);
}
function verificarCaracteresEspeciales(password) {
    $(".js-password").empty();
    var letras = "!#$%&/()=?¡";
    var tieneCaracter = false;
    for (var i = 0; i < 10; i++) {
        if (password.includes(letras[i])) {
            tieneCaracter = true;
            break;
        }
    }
    if (tieneCaracter === false) {
        $(".js-password").append("Mínimo 6 caractreres. Números, letras y al menos 1 caracter especial.</small><div class='card text-white bg-danger' style='max-width: 20rem;'>\n\
<div class='card-header'>La contraseña debe tener caracteres especiales</div></div>");
        throw new Error("La contraseña debe tener caracteres especiales.");
    }
}

function verificarAbc(password) {
    $(".js-password").empty();
    var tieneCaracter = false;
    var letras = "abcdefghijklmnñopqrstuvwxyz";
    for (var i = 0, max = letras.length; i < max; i++) {
        if (password.includes(letras[i])) {
            tieneCaracter = true;
            break;
        }
    }
    if (tieneCaracter === false) {
        {
            $(".js-password").append("Mínimo 6 caractreres. Números, letras y al menos 1 caracter especial.</small><div class='card text-white bg-danger' style='max-width: 20rem;\n\
'><div class='card-header'>La contraseña debe poseer letras.</div></div>");
            throw new Error("La contraseña debe tener letras.");
        }
    }
    }
    function verificarPasswordConNumeros(password) {
        $(".js-password").empty();
        var letras = "01234567989";
        if (password.match(letras)) {
        } else {
            $(".js-password").append("Mínimo 6 caractreres. Números, letras y al menos 1 caracter especial.</small><div class='card text-white bg-danger' style='max-width: 20rem;'>\n\
<div class='card-header'>La contraseña debe tener números</div></div>");
            throw new Error("La contraseña debe tener numero/s.");
        }
    }

    function verificarCantidadDeCaracteresPassword(password) {
        $(".js-password").empty();
        if (password.length < 6) {
            $(".js-password").append("Mínimo 6 caractreres. Números, letras y al menos 1 caracter especial.</small><div class='card text-white bg-danger' style='max-width: 20rem;\n\
'><div class='card-header'>La contraseña debe tener mas de 6 caracteres</div></div>");
            throw new Error("La contraseña debe tener menos de 6 caracteres.");
        }
    }



    function verificarInputMailVacio(mail) {
        $(".errorMail").remove();
        if (mail === "") {
            $(".js-email").html("¡No puede haber usuarios repetidos!</small><div class='card errorMail text-white bg-danger' style='max-width: 20rem;'><div class='card-header'>El campo mail esta vacío.</div></div>");
            throw new Error("El campo mail esta vacio.");
        }
    }

    function verificarFormatoMail(mail) {
        $(".errorMail").remove();
        var atpos = mail.indexOf("@");
        var dotpos = mail.lastIndexOf(".");
        if (atpos > 0 && dotpos > atpos + 1 && dotpos + 2 < mail.length) {

        } else {
            $(".js-email").html("¡No puede haber usuarios repetidos!</small><div class='card errorMail text-white bg-danger' style='max-width: 20rem;'><div class='card-header'>El mail debe contener @ y luego .</div></div>");
            throw new Error("El formato del mail es erróneo.");
        }
    }

    function reiniciarInputs() {
        $(".js-email-usuario").val("");
        $("#password").val("");
        $("#password-verificar").val("");
    }


    $(document).ready(init); 